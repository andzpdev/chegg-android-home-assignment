package homework.chegg.com.chegghomework.data.remote.loader

import com.nhaarman.mockitokotlin2.*
import homework.chegg.com.chegghomework.data.remote.api.CheggApi
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteMapperA
import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseA
import homework.chegg.com.chegghomework.data.remote.reponse.Story
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class ItemsLoaderATest {

    private lateinit var loader: ItemsLoaderA

    private val apiMock: CheggApi = mock()
    private val apiResponseMock: Response<ItemResponseA> = mock()
    private val mapperMock: RemoteMapperA = mock()

    private val successfulItemResponseA = ItemResponseA(listOf(Story("IMG_URL", "SUBTITLE", "TITLE")))
    private val mapperResultForResponse = listOf(Item("TITLE", "SUBTITLE", "IMG_URL"))

    lateinit var loadResult: Result<List<Item>>

    @Before
    fun setUp() {
        loader = ItemsLoaderA(apiMock, mapperMock)
    }

    @Test
    fun `test load data from API - api response is Success`() = runBlockingTest {
        `given Chegg API that return successful response with one Story in body `()
        `given mapper that parse response into list of items`()

        `when get reponse from loader`()

        `that verify that getItemsFromA was called once`()
        `that verify that response isSuccessful`()
        `that verify that map was called with body response from API call`()
        `that verify result of load() is Success with expected items list`()
    }

    @Test
    fun `test load data from API - api response is Error `() = runBlockingTest {
        `given Chegg API that return error response`()

        `when get reponse from loader`()

        `that verify that getItemsFromA was called once and response return error`()
        `that verify result of load() is Error`()
    }

    @Test
    fun `test load data from API - api throw Exception`() = runBlockingTest {
        `given Chegg API that throws exception`()

        `when get reponse from loader`()

        `that verify that getItemsFromA was called once and response return error`()
        `that verify result of load() is Error`()
    }

    private suspend fun `given Chegg API that return successful response with one Story in body `() {
        whenever(apiMock.getItemsFromA()).thenReturn(apiResponseMock)
        whenever(apiResponseMock.isSuccessful).thenReturn(true)
        whenever(apiResponseMock.body()).thenReturn(successfulItemResponseA)
    }

    private suspend fun `given Chegg API that return error response`() {
        whenever(apiMock.getItemsFromA()).thenReturn(apiResponseMock)
        whenever(apiResponseMock.isSuccessful).thenReturn(false)
        whenever(apiResponseMock.errorBody()).thenReturn(ResponseBody.create(MediaType.parse("TEXT"), "Error"))
    }

    private suspend fun `given Chegg API that throws exception`() {
        whenever(apiMock.getItemsFromA()).thenThrow(IllegalStateException())
    }

    private fun `given mapper that parse response into list of items`() {
        whenever(mapperMock.map(successfulItemResponseA)).thenReturn(mapperResultForResponse)
    }

    private suspend fun `when get reponse from loader`() {
        loadResult = loader.load()
    }

    private suspend fun `that verify that getItemsFromA was called once`() {
        verify(apiMock, times(1)).getItemsFromA()
        verify(apiResponseMock).isSuccessful
    }

    private suspend fun `that verify that getItemsFromA was called once and response return error`() {
        verify(apiMock, times(1)).getItemsFromA()
    }

    private fun `that verify that response isSuccessful`() {
        verify(apiResponseMock, times(1)).isSuccessful
    }

    private fun `that verify that map was called with body response from API call`() {
        verify(mapperMock, times(1)).map(successfulItemResponseA)
    }

    private fun `that verify result of load() is Success with expected items list`() {
        assertThat(loadResult).isEqualTo(Result.Success(mapperResultForResponse))
    }

    private fun `that verify result of load() is Error`() {
        assertThat(loadResult).isInstanceOf(Result.Error::class.java)
    }
}