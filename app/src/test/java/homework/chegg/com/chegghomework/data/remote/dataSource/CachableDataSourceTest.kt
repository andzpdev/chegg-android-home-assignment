package homework.chegg.com.chegghomework.data.remote.dataSource

import com.nhaarman.mockitokotlin2.*
import homework.chegg.com.chegghomework.data.core.cache.Cache
import homework.chegg.com.chegghomework.data.remote.loader.ItemsLoader
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.assertj.core.api.Assertions.assertThat

import org.junit.Test
import java.io.IOException

@ExperimentalCoroutinesApi
class CachableDataSourceTest {

    private val loaderMock: ItemsLoader = mock()
    private val cacheMock: Cache<List<Item>> = mock()

    private var dataSource: CachableDataSource = CachableDataSource(loaderMock, cacheMock)

    private val itemsList = listOf(Item("TITLE", "DESC", "URL"))
    private val resultSuccessFromCache = Result.Success(itemsList)
    private val resultSuccessFromLoader = Result.Success(itemsList)
    private val resultErrorFromLoader = Result.Error(IOException())

    lateinit var dataSourceResult: Result<List<Item>>

    @Test
    fun `test get items - cache contains items`() = runBlockingTest {
        `given cache with items`()

        `when get result from dataSource`()

        `than verify that loader was never called`()
        `than verify that get from cache was called once`()
        `than verify that dataSource get result is Success`()
    }

    @Test
    fun `test get items - cache does not contain items - load from loader Success and update cache`() = runBlockingTest {
        `given cache with no items`()
        `given loader return result Success`()

        `when get result from dataSource`()

        `than verify that get from cache was called once`()
        `than verify that loader was called once`()
        `than verify that cache was updated`()
        `than verify that dataSource get result is Success`()
    }

    @Test
    fun `test get items - cache does not contain items - load from loader Error`() = runBlockingTest {
        `given cache with no items`()
        `given loader return result Error`()

        `when get result from dataSource`()

        `than verify that get from cache was called once`()
        `than verify that loader was called once`()
        `than verify that cache was never updated`()
        `than verify that dataSource get result is Error`()
    }

    private fun `given cache with items`() {
        whenever(cacheMock[any()]).thenReturn(itemsList)
    }

    private fun `given cache with no items`() {
        whenever(cacheMock[any()]).thenReturn(null)
    }

    private suspend fun `given loader return result Success`() {
        whenever(loaderMock.load()).thenReturn(resultSuccessFromLoader)
    }

    private suspend fun `given loader return result Error`() {
        whenever(loaderMock.load()).thenReturn(resultErrorFromLoader)
    }

    private suspend fun `when get result from dataSource`() {
        dataSourceResult = dataSource.get()
    }

    private suspend fun `than verify that loader was never called`() {
        verify(loaderMock, never()).load()
    }

    private suspend fun `than verify that loader was called once`() {
        verify(loaderMock, times(1)).load()
    }

    private fun `than verify that get from cache was called once`() {
        verify(cacheMock, times(1))[any()]
    }

    private fun `than verify that cache was updated`() {
        verify(cacheMock, times(1))[any()] = eq(resultSuccessFromLoader.data)
    }

    private fun `than verify that cache was never updated`() {
        verify(cacheMock, never())[any()] = any()
    }

    private fun `than verify that dataSource get result is Success`() {
        assertThat(dataSourceResult).isEqualTo(resultSuccessFromCache)
    }

    private fun `than verify that dataSource get result is Error`() {
        assertThat(dataSourceResult).isEqualTo(resultErrorFromLoader)
    }

}