package homework.chegg.com.chegghomework.data.remote.loader

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import homework.chegg.com.chegghomework.data.remote.api.CheggApi
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteMapperB
import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseB
import homework.chegg.com.chegghomework.data.remote.reponse.Metadata
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class ItemsLoaderBTest {

    private lateinit var loader: ItemsLoaderB

    private val apiMock: CheggApi = mock()
    private val apiResponseMock: Response<ItemResponseB> = mock()
    private val mapperMock: RemoteMapperB = mock()

    private val successfulItemResponseB = ItemResponseB(Metadata("this1", emptyList()))
    private val mapperResultForResponse = listOf(Item("TITLE", "SUBTITLE", "IMG_URL"))

    lateinit var loadResult: Result<List<Item>>

    @Before
    fun setUp() {
        loader = ItemsLoaderB(apiMock, mapperMock)
    }

    @Test
    fun `test load data from API - api response is Success`() = runBlockingTest {
        `given Chegg API that return successful response with one Story in body `()
        `given mapper that parse response into list of items`()

        `when get reponse from loader`()

        `that verify that getItemsFromA was called once`()
        `that verify that response isSuccessful`()
        `that verify that map was called with body response from API call`()
        `that verify result of load() is Success with expected items list`()
    }

    @Test
    fun `test load data from API - api response is Error `() = runBlockingTest {
        `given Chegg API that return error response`()

        `when get reponse from loader`()

        `that verify that getItemsFromA was called once and response return error`()
        `that verify result of load() is Error`()
    }

    @Test
    fun `test load data from API - api throw Exception`() = runBlockingTest {
        `given Chegg API that throws exception`()

        `when get reponse from loader`()

        `that verify that getItemsFromA was called once and response return error`()
        `that verify result of load() is Error`()
    }

    private suspend fun `given Chegg API that return successful response with one Story in body `() {
        whenever(apiMock.getItemsFromB()).thenReturn(apiResponseMock)
        whenever(apiResponseMock.isSuccessful).thenReturn(true)
        whenever(apiResponseMock.body()).thenReturn(successfulItemResponseB)
    }

    private suspend fun `given Chegg API that return error response`() {
        whenever(apiMock.getItemsFromB()).thenReturn(apiResponseMock)
        whenever(apiResponseMock.isSuccessful).thenReturn(false)
        whenever(apiResponseMock.errorBody()).thenReturn(ResponseBody.create(MediaType.parse("TEXT"), "Error"))
    }

    private suspend fun `given Chegg API that throws exception`() {
        whenever(apiMock.getItemsFromB()).thenThrow(IllegalStateException())
    }

    private fun `given mapper that parse response into list of items`() {
        whenever(mapperMock.map(successfulItemResponseB)).thenReturn(mapperResultForResponse)
    }

    private suspend fun `when get reponse from loader`() {
        loadResult = loader.load()
    }

    private suspend fun `that verify that getItemsFromA was called once`() {
        verify(apiMock, times(1)).getItemsFromB()
        verify(apiResponseMock).isSuccessful
    }

    private suspend fun `that verify that getItemsFromA was called once and response return error`() {
        verify(apiMock, times(1)).getItemsFromB()
    }

    private fun `that verify that response isSuccessful`() {
        verify(apiResponseMock, times(1)).isSuccessful
    }

    private fun `that verify that map was called with body response from API call`() {
        verify(mapperMock, times(1)).map(successfulItemResponseB)
    }

    private fun `that verify result of load() is Success with expected items list`() {
        assertThat(loadResult).isEqualTo(Result.Success(mapperResultForResponse))
    }

    private fun `that verify result of load() is Error`() {
        assertThat(loadResult).isInstanceOf(Result.Error::class.java)
    }
}