package homework.chegg.com.chegghomework.data.remote

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import homework.chegg.com.chegghomework.data.core.ItemsDataSource
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.io.IOException

@ExperimentalCoroutinesApi
class CompositeRemoteDataSourceTest {
    private val dataSourceAMock: ItemsDataSource = mock()
    private val dataSourceBMock: ItemsDataSource = mock()
    private val dataSourceCMock: ItemsDataSource = mock()

    private val compositeDataSource = CompositeRemoteDataSource(listOf(dataSourceAMock, dataSourceBMock, dataSourceCMock))

    private val itemA = Item("A", "A", "A")
    private val itemB = Item("B", "B", "B")
    private val itemC = Item("C", "C", "C")

    private val itemsListA = listOf(itemA)
    private val itemsListB = listOf(itemB)
    private val itemsListC = listOf(itemC)

    lateinit var compositeDataSourceResult: Result<List<Item>>


    @Test
    fun `test get items from dataSources ABC and return combined result - all dataSources return Success`() = runBlockingTest {
        `given dataSource A return success with item A`()
        `given dataSource B return success with item B`()
        `given dataSource C return success with item C`()

        `when get result from composite data source`()

        `than verify that get was call once per each A B C data source`()
        `than verify result is Success with items from A B C data sources`()
    }

    @Test
    fun `test get items from dataSources ABC and return combined result - A and C return Success, B - return Error`() = runBlockingTest {
        `given dataSource A return success with item A`()
        `given dataSource B return Error`()
        `given dataSource C return success with item C`()

        `when get result from composite data source`()

        `than verify that get was call once per each A B C data source`()
        `than verify result is Success with items from A C data sources`()
    }


    @Test
    fun `test get items from dataSources ABC - all data source return Error`() = runBlockingTest {
        `given dataSource A return Error`()
        `given dataSource B return Error`()
        `given dataSource C return Error`()

        `when get result from composite data source`()

        `than verify that get was call once per each A B C data source`()
        `than verify result is Success with no items`()
    }


    private suspend fun `given dataSource A return success with item A`() {
        whenever(dataSourceAMock.get()).thenReturn(Result.Success(itemsListA))
    }

    private suspend fun `given dataSource A return Error`() {
        whenever(dataSourceAMock.get()).thenReturn(Result.Error(IOException()))
    }

    private suspend fun `given dataSource B return success with item B`() {
        whenever(dataSourceBMock.get()).thenReturn(Result.Success(itemsListB))
    }

    private suspend fun `given dataSource B return Error`() {
        whenever(dataSourceBMock.get()).thenReturn(Result.Error(IOException()))
    }

    private suspend fun `given dataSource C return success with item C`() {
        whenever(dataSourceCMock.get()).thenReturn(Result.Success(itemsListC))
    }

    private suspend fun `given dataSource C return Error`() {
        whenever(dataSourceCMock.get()).thenReturn(Result.Error(IOException()))
    }

    private suspend fun `when get result from composite data source`() {
        compositeDataSourceResult = compositeDataSource.get()
    }

    private suspend fun `than verify that get was call once per each A B C data source`() {
        verify(dataSourceAMock, times(1)).get()
        verify(dataSourceBMock, times(1)).get()
        verify(dataSourceCMock, times(1)).get()
    }

    private fun `than verify result is Success with items from A B C data sources`() {
        assertThat(compositeDataSourceResult).isEqualTo(Result.Success(listOf(itemA, itemB, itemC)))
    }

    private fun `than verify result is Success with items from A C data sources`() {
        assertThat(compositeDataSourceResult).isEqualTo(Result.Success(listOf(itemA, itemC)))
    }

    private fun `than verify result is Success with no items`() {
        assertThat(compositeDataSourceResult).isEqualTo(Result.Success(emptyList<Item>()))
    }

}