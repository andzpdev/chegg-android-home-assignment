package homework.chegg.com.chegghomework.data.core.cache

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import homework.chegg.com.chegghomework.utils.TimeUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.util.concurrent.TimeUnit

class ExpirableCacheTest {

    private val KEY = "KEY"
    private val VALUE = "VALUE"

    private val flushInterval_1min = TimeUnit.MINUTES.toMillis(1)
    private val timeUtilsMock: TimeUtils = mock()

    lateinit var cache: ExpirableCache<String>

    private var cacheSize: Int = -1
    private var valueFromCache: String? = null

    @Test
    fun `test cache size with empty cache`() {
        `given empty cache`()

        `when check cache size`()

        `than verify cache size is 0`()
    }

    @Test
    fun `test cache size with not empty cache`() {
        `given one element cache`()

        `when check cache size`()

        `than verify cache size is 1`()
    }

    @Test
    fun `test put value to cache`() {
        `given one element cache`()

        `when get value from cache`()

        `than verify value received from cache is equal to expected`()
    }


    @Test
    fun `test remove value from cache`() {
        `given one element cache`()

        `when remove value from cache`()

        `than verify cache is empty`()

    }

    @Test
    fun `test clear remove all values from cache`() {
        `given one element cache`()

        `when clear values from cache`()

        `than verify cache is empty`()
    }

    @Test
    fun `test get element from empty cache`() {
        `given empty cache`()

        `when get value from cache`()

        `than verify value received from cache is null`()
    }

    @Test
    fun `test get expired element from cache`() {
        `given one element cache`()
        `given system clock return time after cache expired`()

        `when get value from cache`()

        `than verify cache is empty`()
        `than verify value received from cache is null`()
    }

    @Test
    fun `test get not expired element from cache`() {
        `given one element cache`()
        `given system clock return time before cache expired`()

        `when get value from cache`()
        `when check cache size`()

        `than verify cache size is 1`()
        `than verify value received from cache is equal to expected`()
    }


    private fun `given empty cache`() {
        cache = ExpirableCache(flushInterval = flushInterval_1min, timeUtils = timeUtilsMock)
    }

    private fun `given one element cache`() {
        cache = ExpirableCache(flushInterval = flushInterval_1min, timeUtils = timeUtilsMock)
        cache[KEY] = "VALUE"
    }

    private fun `given system clock return time after cache expired`() {
        whenever(timeUtilsMock.currentTimeMillis()).thenReturn(9999999999999)
    }

    private fun `given system clock return time before cache expired`() {
        whenever(timeUtilsMock.currentTimeMillis()).thenReturn(0)
    }

    private fun `when get value from cache`() {
        valueFromCache = cache[KEY]
    }

    private fun `when check cache size`() {
        cacheSize = cache.size
    }

    private fun `when remove value from cache`() {
        cache.remove(KEY)
    }

    private fun `when clear values from cache`() {
        cache.clear()
    }

    private fun `than verify cache size is 0`() {
        assertThat(cacheSize).isEqualTo(0)
    }

    private fun `than verify cache is empty`() {
        assertThat(cache.size).isEqualTo(0)
    }

    private fun `than verify cache size is 1`() {
        assertThat(cacheSize).isEqualTo(1)
    }

    private fun `than verify value received from cache is equal to expected`() {
        assertThat(valueFromCache).isEqualTo(VALUE)
    }

    private fun `than verify value received from cache is null`() {
        assertThat(valueFromCache).isNull()
    }

}