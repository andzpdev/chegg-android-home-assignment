package homework.chegg.com.chegghomework.domain.useCase

import homework.chegg.com.chegghomework.data.ItemsRepo
import homework.chegg.com.chegghomework.domain.entity.Content
import homework.chegg.com.chegghomework.domain.entity.Failure
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import homework.chegg.com.chegghomework.utils.Either
import homework.chegg.com.chegghomework.utils.logDebug
import homework.chegg.com.chegghomework.utils.logWarning

class GetContentItems(private val repo: ItemsRepo) : BaseUseCase<Content, GetContentItems.NoParams>() {

    override suspend fun run(params: NoParams): Either<Failure.FeatureFailure, Content> {
        return when (val result = repo.getAll()) {
            is Result.Success -> onSuccessResult(result)
            is Result.Error -> onErrorResult(result)
        }
    }

    private fun onErrorResult(result: Result.Error): Either.Left<Failure.FeatureFailure> {
        logWarning("onErrorResult", result.exception)
        return Either.Left(Failure.FeatureFailure(result.exception))
    }

    private fun onSuccessResult(result: Result.Success<List<Item>>): Either.Right<Content> {
        val items = result.data
        logDebug("onSuccess: items size [${items.size}]")
        return Either.Right(Content(items))
    }

    object NoParams : Any()
}