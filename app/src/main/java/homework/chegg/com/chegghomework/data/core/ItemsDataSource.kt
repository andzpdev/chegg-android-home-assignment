package homework.chegg.com.chegghomework.data.core

import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result

interface ItemsDataSource {
    suspend fun get(): Result<List<Item>>
}