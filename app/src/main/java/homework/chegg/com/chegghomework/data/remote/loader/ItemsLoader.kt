package homework.chegg.com.chegghomework.data.remote.loader

import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result

interface ItemsLoader {
    suspend fun load(): Result<List<Item>>
}