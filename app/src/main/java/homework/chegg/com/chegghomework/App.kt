package homework.chegg.com.chegghomework

import android.app.Application
import com.facebook.stetho.Stetho
import homework.chegg.com.chegghomework.di.data.remote.remoteDataSourceModule
import homework.chegg.com.chegghomework.di.data.remote.retrofitModule
import homework.chegg.com.chegghomework.di.data.repoModule
import homework.chegg.com.chegghomework.di.domain.useCaseModule
import homework.chegg.com.chegghomework.di.presentation.viewModelModule
import homework.chegg.com.chegghomework.di.utils.utilsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        initLogs()
        initStetho()
        initDi()
    }

    private fun initLogs() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
    }

    private fun initDi() {
        startKoin {
            androidContext(applicationContext)
            modules(
                    listOf(
                            utilsModule,
                            retrofitModule,
                            remoteDataSourceModule,
                            repoModule,
                            useCaseModule,
                            viewModelModule
                    )
            )
        }
    }
}