package homework.chegg.com.chegghomework.data.remote.mapper

import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseA
import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseB
import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseC
import homework.chegg.com.chegghomework.domain.entity.Item

interface RemoteMapperA {
    fun map(response: ItemResponseA): List<Item> {
        return response.stories
                .map { story ->
                    Item(
                            title = story.title,
                            desc = story.subtitle,
                            imageUrl = story.imageUrl
                    )
                }
    }
}

interface RemoteMapperB {
    fun map(response: ItemResponseB): List<Item> {
        return response.metadata.innerdata.map { data ->
            Item(
                    title = data.articlewrapper.header,
                    desc = data.articlewrapper.description,
                    imageUrl = data.picture
            )
        }
    }
}

interface RemoteMapperC {
    fun map(responses: List<ItemResponseC>): List<Item> {
        return responses.map { response ->
            Item(
                    title = response.topLine,
                    desc = response.subLine1 + response.subline2,
                    imageUrl = response.image
            )
        }
    }
}

object RemoteItemsMapper : RemoteMapperA, RemoteMapperB, RemoteMapperC