package homework.chegg.com.chegghomework.data.remote.reponse

data class ItemResponseA(
    val stories: List<Story>
)

data class Story(
        val imageUrl: String,
        val subtitle: String,
        val title: String
)