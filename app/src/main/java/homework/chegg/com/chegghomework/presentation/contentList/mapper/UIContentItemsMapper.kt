package homework.chegg.com.chegghomework.presentation.contentList.mapper

import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.presentation.contentList.entity.UIContentItem

interface UIContentItemsMapper {
    fun map(items: List<Item>): List<UIContentItem> {
        return items.map { item ->
            UIContentItem(
                    title = item.title,
                    desc = item.desc,
                    imageUrl = item.imageUrl
            )
        }
    }
}

object UIMapper : UIContentItemsMapper