@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di.data.remote

import homework.chegg.com.chegghomework.Const
import homework.chegg.com.chegghomework.data.core.ItemsDataSource
import homework.chegg.com.chegghomework.data.core.cache.ExpirableCache
import homework.chegg.com.chegghomework.data.remote.CompositeRemoteDataSource
import homework.chegg.com.chegghomework.data.remote.api.CheggApi
import homework.chegg.com.chegghomework.data.remote.dataSource.CachableDataSource
import homework.chegg.com.chegghomework.data.remote.loader.ItemsLoaderA
import homework.chegg.com.chegghomework.data.remote.loader.ItemsLoaderB
import homework.chegg.com.chegghomework.data.remote.loader.ItemsLoaderC
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteItemsMapper
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteMapperA
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteMapperB
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteMapperC
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.utils.TimeUtils
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val remoteDataSourceModule = module {
    factory<RemoteMapperA> { RemoteItemsMapper }
    factory<RemoteMapperB> { RemoteItemsMapper }
    factory<RemoteMapperC> { RemoteItemsMapper }

    factory<ItemsLoaderA> { ItemsLoaderA(get<CheggApi>(), get<RemoteMapperA>()) }
    factory<ItemsLoaderB> { ItemsLoaderB(get<CheggApi>(), get<RemoteMapperB>()) }
    factory<ItemsLoaderC> { ItemsLoaderC(get<CheggApi>(), get<RemoteMapperC>()) }


    factory<ExpirableCache<List<Item>>> { (cacheStaleTime: Long) -> ExpirableCache(flushInterval = cacheStaleTime, timeUtils = get<TimeUtils>()) }

    factory<List<ItemsDataSource>> {
        listOf(
                CachableDataSource(get<ItemsLoaderA>(), get<ExpirableCache<List<Item>>> { parametersOf(Const.DATA_SOURCE_A_CACHE_STALE_TIME) }),
                CachableDataSource(get<ItemsLoaderB>(), get<ExpirableCache<List<Item>>> { parametersOf(Const.DATA_SOURCE_B_CACHE_STALE_TIME) }),
                CachableDataSource(get<ItemsLoaderC>(), get<ExpirableCache<List<Item>>> { parametersOf(Const.DATA_SOURCE_C_CACHE_STALE_TIME) })
        )
    }

    single<CompositeRemoteDataSource> { CompositeRemoteDataSource(get<List<ItemsDataSource>>()) }
}