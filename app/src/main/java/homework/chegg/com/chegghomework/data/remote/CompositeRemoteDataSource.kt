package homework.chegg.com.chegghomework.data.remote

import homework.chegg.com.chegghomework.data.core.ItemsDataSource
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import homework.chegg.com.chegghomework.utils.parallelMap

class CompositeRemoteDataSource(private val dataSources: List<ItemsDataSource>) : ItemsDataSource {
    override suspend fun get(): Result<List<Item>> {
        return dataSources
                .parallelMap { dataSource -> dataSource.get() }
                .filterIsInstance<Result.Success<List<Item>>>()
                .flatMap { successResult -> successResult.data }
                .let { combinedItems -> Result.Success(combinedItems) }
    }
}

