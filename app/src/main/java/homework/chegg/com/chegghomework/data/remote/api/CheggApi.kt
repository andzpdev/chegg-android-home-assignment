package homework.chegg.com.chegghomework.data.remote.api

import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseA
import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseB
import homework.chegg.com.chegghomework.data.remote.reponse.ItemResponseC
import homework.chegg.com.chegghomework.Const
import homework.chegg.com.chegghomework.Const.DATA_SOURCE_A_PATH
import retrofit2.Response
import retrofit2.http.GET

interface CheggApi {
    @GET("/$DATA_SOURCE_A_PATH")
    suspend fun getItemsFromA(): Response<ItemResponseA>

    @GET("/${Const.DATA_SOURCE_B_PATH}")
    suspend fun getItemsFromB(): Response<ItemResponseB>

    @GET("/${Const.DATA_SOURCE_C_PATH}")
    suspend fun getItemsFromC(): Response<List<ItemResponseC>>
}