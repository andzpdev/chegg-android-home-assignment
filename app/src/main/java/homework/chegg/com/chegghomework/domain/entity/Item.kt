package homework.chegg.com.chegghomework.domain.entity

data class Content(private val items:  List<Item>) : List<Item> by items

data class Item(
        val title: String,
        val desc: String,
        val imageUrl: String
)