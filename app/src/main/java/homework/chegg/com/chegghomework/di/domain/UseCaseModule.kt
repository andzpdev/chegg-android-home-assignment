@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di.domain

import homework.chegg.com.chegghomework.data.ItemsRepo
import homework.chegg.com.chegghomework.domain.useCase.GetContentItems
import org.koin.dsl.module

val useCaseModule = module {
    single<GetContentItems> { GetContentItems(get<ItemsRepo>()) }
}