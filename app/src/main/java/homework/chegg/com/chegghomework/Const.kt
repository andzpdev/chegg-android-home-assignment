package homework.chegg.com.chegghomework

import java.util.concurrent.TimeUnit

object Const {
    const val BASE_URL = "http://chegg-mobile-promotioms.s3.amazonaws.com/"
    private const val ANDROID_HOME_WORK_PATH = "android/homework/"

    const val DATA_SOURCE_A_PATH = ANDROID_HOME_WORK_PATH + "android_homework_datasourceA.json"
    const val DATA_SOURCE_B_PATH = ANDROID_HOME_WORK_PATH + "android_homework_datasourceB.json"
    const val DATA_SOURCE_C_PATH = ANDROID_HOME_WORK_PATH + "android_homework_datasourceC.json"

    val DATA_SOURCE_A_CACHE_STALE_TIME: Long = TimeUnit.MINUTES.toMillis(5)
    val DATA_SOURCE_B_CACHE_STALE_TIME: Long = TimeUnit.MINUTES.toMillis(30)
    val DATA_SOURCE_C_CACHE_STALE_TIME: Long = TimeUnit.MINUTES.toMillis(60)
}
