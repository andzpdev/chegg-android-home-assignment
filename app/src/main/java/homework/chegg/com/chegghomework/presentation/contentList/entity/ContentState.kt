package homework.chegg.com.chegghomework.presentation.contentList.entity

sealed class UIContentState {
    object Loading : UIContentState()
    data class Error(val message: String?) : UIContentState()
    data class Success(val items: List<UIContentItem>) : UIContentState()
}