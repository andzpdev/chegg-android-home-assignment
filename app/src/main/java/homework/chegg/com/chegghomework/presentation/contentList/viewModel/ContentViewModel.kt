package homework.chegg.com.chegghomework.presentation.contentList.viewModel

import androidx.lifecycle.*
import homework.chegg.com.chegghomework.domain.entity.Content
import homework.chegg.com.chegghomework.domain.entity.Failure
import homework.chegg.com.chegghomework.domain.useCase.GetContentItems
import homework.chegg.com.chegghomework.presentation.contentList.entity.UIContentState
import homework.chegg.com.chegghomework.presentation.contentList.mapper.UIContentItemsMapper
import homework.chegg.com.chegghomework.utils.logDebug
import homework.chegg.com.chegghomework.utils.logWarning

class ContentViewModel(
        private val getContent: GetContentItems,
        private val mapper: UIContentItemsMapper
) : ViewModel(), LifecycleObserver {
    val state = MutableLiveData<UIContentState>()

    @OnLifecycleEvent(value = Lifecycle.Event.ON_RESUME)
    fun loadContent() {
        logDebug("loadContent]")
        state.postValue(UIContentState.Loading)
        getContent(viewModelScope, GetContentItems.NoParams) { it.either(::handleFailure, ::handleSuccess) }
    }

    private fun handleFailure(failure: Failure) {
        logDebug("handleFailure: [${failure.javaClass.simpleName}]")
        when (failure) {
            is Failure.FeatureFailure -> state.postValue(UIContentState.Error(failure.exception.message))
            else -> logWarning("handleFailure: Unexpected failure [$failure]")
        }
    }

    private fun handleSuccess(content: Content) {
        logDebug("handleSuccess, items size =  [${content.size}]]")
        postContentItems(content)
    }

    private fun postContentItems(content: Content) {
        val uiItems = mapper.map(content)
        logDebug("postContentItems: uiItems size [${uiItems.size}]")
        state.postValue(UIContentState.Success(uiItems))
    }
}