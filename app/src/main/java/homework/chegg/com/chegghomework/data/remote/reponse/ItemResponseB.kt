package homework.chegg.com.chegghomework.data.remote.reponse

data class ItemResponseB(
    val metadata: Metadata
)

data class Metadata(
        val `this`: String,
        val innerdata: List<Innerdata>
)


data class Innerdata(
        val articlewrapper: Articlewrapper,
        val aticleId: Int,
        val picture: String
)

data class Articlewrapper(
        val description: String,
        val header: String
)