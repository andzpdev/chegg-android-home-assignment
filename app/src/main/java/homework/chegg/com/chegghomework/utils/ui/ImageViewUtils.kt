package homework.chegg.com.chegghomework.utils.ui

import android.widget.ImageView

fun ImageView.loadUrl(url:String){
    GlideApp.with(this)
            .load(url)
            .into(this)
}