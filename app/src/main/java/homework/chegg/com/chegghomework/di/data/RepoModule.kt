@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di.data

import homework.chegg.com.chegghomework.data.ItemsRepo
import homework.chegg.com.chegghomework.data.ItemsRepoImpl
import homework.chegg.com.chegghomework.data.remote.CompositeRemoteDataSource
import org.koin.dsl.module

val repoModule = module {
    single<ItemsRepo> { ItemsRepoImpl(get<CompositeRemoteDataSource>()) }
}