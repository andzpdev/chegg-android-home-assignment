package homework.chegg.com.chegghomework.data.remote.loader

import homework.chegg.com.chegghomework.data.remote.api.CheggApi
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteItemsMapper
import homework.chegg.com.chegghomework.data.remote.mapper.RemoteMapperC
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import homework.chegg.com.chegghomework.Const
import homework.chegg.com.chegghomework.utils.logDebug
import homework.chegg.com.chegghomework.utils.logWarning
import java.io.IOException

class ItemsLoaderC(
        private val api: CheggApi,
        private val mapper: RemoteMapperC = RemoteItemsMapper
) : ItemsLoader {
    override suspend fun load(): Result<List<Item>> {
        logDebug("[$this]: load called")
        return try {
            val itemsResponse = api.getItemsFromC()
            val body = itemsResponse.body()
            if (itemsResponse.isSuccessful && body != null) {
                val items = mapper.map(body)
                logDebug("load: Successful return items size [${items.size}]")
                Result.Success(items)
            } else {
                Result.Error(IOException("Error to fetch content ${itemsResponse.errorBody()}"))
            }
        } catch (e: Exception) {
            logWarning("fetchFromServer: Error from API", e)
            Result.Error(e)
        }
    }

    override fun toString(): String {
        return "${javaClass.simpleName}: dataSourcePath [${Const.DATA_SOURCE_C_PATH}]"
    }
}