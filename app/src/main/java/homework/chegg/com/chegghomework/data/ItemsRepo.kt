package homework.chegg.com.chegghomework.data

import homework.chegg.com.chegghomework.data.core.ItemsDataSource
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import homework.chegg.com.chegghomework.utils.logDebug

interface ItemsRepo {
    suspend fun getAll(): Result<List<Item>>
}

class ItemsRepoImpl(private val remote: ItemsDataSource) : ItemsRepo {

    override suspend fun getAll(): Result<List<Item>> {
        return remote.get().also {
            logDebug("getAll: return result [${it.javaClass.simpleName}]")
        }
    }
}

