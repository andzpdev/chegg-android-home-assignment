package homework.chegg.com.chegghomework.data.remote.dataSource

import homework.chegg.com.chegghomework.data.core.ItemsDataSource
import homework.chegg.com.chegghomework.data.core.cache.Cache
import homework.chegg.com.chegghomework.data.core.cache.ExpirableCache
import homework.chegg.com.chegghomework.data.remote.loader.ItemsLoader
import homework.chegg.com.chegghomework.domain.entity.Item
import homework.chegg.com.chegghomework.domain.entity.Result
import homework.chegg.com.chegghomework.utils.logDebug


private const val ITEMS_CACHE_KEY = "homework.chegg.com.chegghomework.ITEMS_CACHE_KEY"

class CachableDataSource(
        private val loader: ItemsLoader,
        private val cache: Cache<List<Item>>
) : ItemsDataSource {

    override suspend fun get(): Result<List<Item>> {
        val cachedItems = cache[ITEMS_CACHE_KEY]
        return if (cachedItems != null) {
            logDebug("Result from cache: size [${cachedItems.size}]")
            Result.Success(cachedItems)
        } else {
            loader.load().also { result ->
                logDebug("load: result [${result.javaClass.simpleName}] from [$loader")
                when (result) {
                    is Result.Success<List<Item>> -> updateCache(result.data)
                }
            }
        }
    }

    private fun updateCache(items: List<Item>) {
        logDebug("updateCache: items size [${items.size}]")
        cache[ITEMS_CACHE_KEY] = items
    }
}


