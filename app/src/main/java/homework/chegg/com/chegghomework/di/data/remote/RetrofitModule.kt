@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di.data.remote

import com.facebook.stetho.okhttp3.StethoInterceptor
import homework.chegg.com.chegghomework.data.remote.api.CheggApi
import homework.chegg.com.chegghomework.Const
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val retrofitModule = module {
    factory<OkHttpClient> {
        OkHttpClient().newBuilder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()
    }

    factory<Retrofit> {
        Retrofit.Builder()
                .client(get<OkHttpClient>())
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
    }

    single <CheggApi> { get<Retrofit>().create(CheggApi::class.java) }
}