package homework.chegg.com.chegghomework.data.core.cache

import homework.chegg.com.chegghomework.utils.TimeUtils
import homework.chegg.com.chegghomework.utils.logDebug
import java.util.concurrent.TimeUnit

class ExpirableCache<T>(
        private val delegate: Cache<T> = PerpetualCache(),
        private val flushInterval: Long = TimeUnit.MINUTES.toMillis(1),
        private val timeUtils: TimeUtils
) : Cache<T> {
    private var lastFlushTime = timeUtils.currentTimeMillis()

    override val size: Int
        get() = delegate.size

    override fun set(key: Any, value: T) {
        delegate[key] = value
    }

    override fun remove(key: Any): T? {
        recycle()
        return delegate.remove(key)
    }

    override fun get(key: Any): T? {
        recycle()
        return delegate[key]
    }

    override fun clear() = delegate.clear()

    private fun recycle() {
        val shouldRecycle = timeUtils.currentTimeMillis() - lastFlushTime >= flushInterval
        logDebug("recycle: shouldRecycle [$shouldRecycle]")
        if (!shouldRecycle) return
        delegate.clear()
    }
}