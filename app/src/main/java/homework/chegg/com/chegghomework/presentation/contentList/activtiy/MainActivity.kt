package homework.chegg.com.chegghomework.presentation.contentList.activtiy

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.presentation.contentList.adapter.ContentAdapter
import homework.chegg.com.chegghomework.presentation.contentList.entity.UIContentItem
import homework.chegg.com.chegghomework.presentation.contentList.entity.UIContentState
import homework.chegg.com.chegghomework.presentation.contentList.viewModel.ContentViewModel
import homework.chegg.com.chegghomework.utils.logDebug
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val contentViewModel: ContentViewModel by viewModel()
    private val contentAdapter = ContentAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buildUI()
        initViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                onRefreshData()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun buildUI() {
        setupToolbar()
        setupRecyclerView()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar as? Toolbar)
    }

    private fun setupRecyclerView() {
        with(my_recycler_view){
            adapter = contentAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun initViewModel() {
        with(contentViewModel) {
            lifecycle.addObserver(this)
            state.observe(this@MainActivity, Observer { onContentStateChange(it) })
        }
    }

    private fun onContentStateChange(state: UIContentState?) {
        logDebug("onContentStateChange: state[${state?.javaClass?.name}]")
        when (state) {
            is UIContentState.Loading -> showLoading()
            is UIContentState.Error -> showError(state.message)
            is UIContentState.Success -> updateContentList(state.items)
        }
    }

    private fun showLoading() {
        logDebug("showLoading")
        progress_bar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        logDebug("hideLoading")
        progress_bar.visibility = View.GONE
    }

    private fun showError(message: String?) {
        logDebug("showError: message [$message]")
        hideLoading()
        Toast.makeText(this, "fail to load content", Toast.LENGTH_SHORT).show()
    }

    private fun updateContentList(items: List<UIContentItem>) {
        logDebug("updateContentList: items size [$items]")
        contentAdapter.items = items
        hideLoading()
    }

    private fun onRefreshData() {
        logDebug("onRefreshData")
        contentViewModel.loadContent()
    }
}
