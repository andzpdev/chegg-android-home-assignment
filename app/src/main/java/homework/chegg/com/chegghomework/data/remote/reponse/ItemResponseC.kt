package homework.chegg.com.chegghomework.data.remote.reponse

data class ItemResponseC(
    val image: String,
    val subLine1: String,
    val subline2: String,
    val topLine: String
)