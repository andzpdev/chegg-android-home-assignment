@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di.presentation

import homework.chegg.com.chegghomework.domain.useCase.GetContentItems
import homework.chegg.com.chegghomework.presentation.contentList.mapper.UIMapper
import homework.chegg.com.chegghomework.presentation.contentList.viewModel.ContentViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel<ContentViewModel> { ContentViewModel(get<GetContentItems>(), UIMapper) }
}