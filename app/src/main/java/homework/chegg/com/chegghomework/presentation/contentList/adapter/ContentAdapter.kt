package homework.chegg.com.chegghomework.presentation.contentList.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.presentation.contentList.entity.UIContentItem
import homework.chegg.com.chegghomework.presentation.core.adapter.AutoUpdatableAdapter
import homework.chegg.com.chegghomework.utils.ui.inflate
import homework.chegg.com.chegghomework.utils.ui.loadUrl
import kotlinx.android.synthetic.main.card_item.view.*
import kotlin.properties.Delegates

class ContentAdapter : RecyclerView.Adapter<ContentAdapter.ViewHolder>(), AutoUpdatableAdapter {

    var items: List<UIContentItem> by Delegates.observable(emptyList()) { _, old, new ->
        autoNotify(old, new) { o, n -> o == n }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.card_item))
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: UIContentItem) = with(itemView) {
            imageView_card_item.loadUrl(item.imageUrl)
            textView_card_item_title.text = item.title
            textView_card_item_subtitle.text = item.desc
        }
    }
}