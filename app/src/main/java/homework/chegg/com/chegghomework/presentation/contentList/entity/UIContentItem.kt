package homework.chegg.com.chegghomework.presentation.contentList.entity

class UIContentItem(
        val title: String,
        val desc: String,
        val imageUrl: String
)