package homework.chegg.com.chegghomework.utils

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

suspend fun <A, B> Iterable<A>.parallelMap(func: suspend (A) -> B): List<B> = coroutineScope {
    map { item -> async { func(item) } }.map { deferred -> deferred.await() }
}