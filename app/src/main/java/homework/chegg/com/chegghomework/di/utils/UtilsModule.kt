@file:Suppress("RemoveExplicitTypeArguments")

package homework.chegg.com.chegghomework.di.utils

import homework.chegg.com.chegghomework.utils.TimeUtils
import org.koin.dsl.module

val utilsModule = module {
    single<TimeUtils> { TimeUtils() }
}

